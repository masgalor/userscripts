#!/bin/bash

########################################################################
############################# Updatescript #############################
########################################################################


########################################################################
######## This script performs package-updates.
######## Basicly it's the same as "apt update && apt upgrade"
########
######## Recommended alias:
######## alias update='/opt/Scripts/update.sh full -print'

########################################################################
######## Define packages that should never be installed.
_IgnoredPackages="firefox-esr"
########################################################################


########################################################################
########  Check if the script was executed by root
########  If it's a normal user activate "sudo" where neccessary
SUDO=''
if (( $EUID != 0 )); then
    SUDO='sudo'
fi
########################################################################

########################################################################
########  Check if additional parameters where supplied
########  Set Variables to store valid options
DeepClean=false
AutoDownload=false
Print=false
Verbose=false

for option in "$@"; do
  case "$option" in
    -dc)
      DeepClean=true
      ;;
    -ad)
      AutoDownload=true
      ;;
    -print)
      Print=true
      ;;
    -v)
      Verbose=true
      ;;
  esac
done
########################################################################

########################################################################
######## Reformat the user defined list of ignored packages
######## Packages that are not present in apt-cache are ignored
IgnoredPackages=""
for package in $_IgnoredPackages; do
    if apt-cache show "$package" 1>/dev/null 2>&1; then
        IgnoredPackages+="$package- "
    fi
done
########################################################################

########################################################################
######## Definition of all modules

######## Clean
######## Clean apt cache an uninstall unuseg packages
_clean(){
  if [ "$DeepClean" = true ]; then
    if [ "$Verbose" = true ]; then
      eval "$SUDO apt-get clean -y"
      eval "$SUDO apt-get autoclean"
    else
      eval "$SUDO apt-get clean -y > /dev/null"
      eval "$SUDO apt-get autoclean > /dev/null"
    fi
  fi
  if [ "$Verbose" = true ]; then
    eval "$SUDO apt-get autoremove -y"
  else
    eval "$SUDO apt-get autoremove -y > /dev/null"
  fi
  if [ "$Print" = true ]; then
    echo "***Quellen und Pakete sind bereinigt***"
  fi
}

######## Download
######## Download new package lists and upgradeable packages
_download(){
  if [ "$Verbose" = true ]; then
    eval "$SUDO apt-get update"
  else
    eval "$SUDO apt-get update > /dev/null"
  fi
  if [ "$AutoDoanload" = true ]; then
    if [ "$Verbose" = true ]; then
      eval "$SUDO apt-get upgrade $IgnoredPackages -dy"
    else
      eval "$SUDO apt-get upgrade $IgnoredPackages -dy > /dev/null"
    fi
  fi
  if [ "$Print" = true ]; then
    echo "***Paketquellen sind aktuell***"
  fi
}

######## Install
######## Install upgradeable packages
_install(){
eval "$SUDO apt-get dist-upgrade $IgnoredPackages"
  if [ "$Print" = true ]; then
    echo "***Pakete sind aktuell***"
  fi
}

######## Announce
######## Announce changes to security software like rkhunter
_announce(){
  if [ -x "/usr/bin/rkhunter" ]; then
    if [ "$Verbose" = true ]; then
      eval "$SUDO rkhunter --propupd"
    else
      eval "$SUDO rkhunter --propupd > /dev/null"
    fi
  fi
  if [ "$Print" = true ]; then
    echo "***Paketinformationen sind bereit gestellt***"
  fi
}
########################################################################

########################################################################
######## Call the requested functions
case "$1" in
  clean)
    _clean
  ;;

  download)
    _download
  ;;

  install)
    _install
  ;;

  announce)
    _announce
  ;;

  full)
    _clean
    sleep 1
    _download
    sleep 1
    _install
    sleep 1
    _announce
  ;;

  finish)
    _install
    sleep 1
    _announce
  ;;

  *)
    echo "Usage $0 {clean|download|install|announce|full|finish}"
    echo "Options:"
    echo "-dc       DeepClean, clear apt cache before updating"
    echo "-ad       AutoDownload, automatically download new packes"
    echo "-print    Print, prints status messages"
    echo "-v        Verbose, shows the output of performed commands"
  ;;
esac
########################################################################

